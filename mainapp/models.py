from django.db import models


class Employee(models.Model):
    POSITION_CHOICES = (("dev", "Разработчик"), ("tmlead", "Тимлидер"), ("prm", "Проектный менеджер"),
                        ("an", "Аналитик"), ("pr", "Проектировщик"), ("cl", "Заказчик"), ("test", "Тестировщик"))

    email = models.EmailField(help_text="Введите e-mail")
    password = models.CharField(max_length=12, help_text="Пароль")
    position = models.CharField(max_length=30, help_text="Должность", choices=POSITION_CHOICES)
    avatar = models.ImageField(upload_to="static/media/img")


class Project(models.Model):
    title = models.CharField(max_length=100, help_text="Название проекта")
    project_info = models.TextField(help_text="Информация по проекту", null=True)
    employees = models.ManyToManyField(Employee, related_name="assigned")
    beginning_date = models.DateField()
    finish_date = models.DateField()
    creator = models.ForeignKey(Employee, help_text="Начальник проекта", on_delete=models.SET_NULL, null=True)


class Stage(models.Model):
    title = models.CharField(max_length=30, help_text="Название этапа")
    description = models.TextField(help_text="Описание и задачи этапа")
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    beginning_date = models.DateField()
    finish_date = models.DateField()


class Task(models.Model):
    STATUSES = (("postponed", "Отложено"), ("in progress", "В процессе"), ("done", "Выполнено"),
                ("rejected", "Не принято"), ("assigned", "Назначена"), ("not assigned", "Не назначена"),
                ("can't reproduce", "Не воспроизводится"), ("testing", "Тестирование"))

    title = models.CharField(max_length=30, help_text="Заголовок задачи")
    aim = models.TextField(help_text="Описание задачи")
    creator = models.ForeignKey(Employee, on_delete=models.SET_NULL, null=True, related_name="maker")
    employees = models.ManyToManyField(Employee, null=True)
    date_created = models.DateField(auto_now=True, help_text="Дата создания")


